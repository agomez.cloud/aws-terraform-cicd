variable "ec2_region" {
  default = "us-east-1"

}

variable "ec2_image" {
  default = "ami-0ed9277fb7eb570c9"

}

variable "ec2_instance_type" {
  default = "t2.micro"

}

variable "ec2_keypair" {
  default = "AWS-Cisco"

}

variable "ec2_tags" {
  default = "Cisco-Demo-Terraform-1"

}

variable "ec2_count" {
  default = "2"

}

#######################

output "instance_ip_addr" {
  value       = aws_instance.OneServer.*.private_ip
  description = "The private IP address of the main instance."

}

output "instance_ips" {
  value = aws_instance.OneServer.*.public_ip
}
